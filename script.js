const btnAdd = document.querySelector('button');
const container = document.querySelector('.container');

/* FUNCTION (default empty) */
const addPostIt = (post = '') => {
    const postIt = document.createElement('div');
    postIt.classList.add("container-postit");
    postIt.innerHTML = `
        <div class="add-postit"></div>
        <div class="border-postit">
            <div class="icons">
                <i class="fas fa-edit edit"></i>
                <i class="fas fa-trash remove"></i>
            </div>
        </div>
        <textarea id="text" name="text" cols="90" rows="14">${post}</textarea>
    `;
    container.appendChild(postIt);
    container.insertBefore(postIt, container.childNodes[0]);

    const textarea = postIt.querySelector('textarea');
    const btnEdit = postIt.querySelector('.edit');
    const btnRemove = postIt.querySelector('.remove');

    textarea.addEventListener("input", () => {
        updateLS();
    });

    btnEdit.addEventListener('click', () => {
        text = postIt.querySelector('textarea');
        text.classList.toggle('editToggle');
    });
    btnRemove.addEventListener('click', () => {
        postIt.remove();
        updateLS();
    });
}


/* LOCAL STORAGE */
const getPostIt = JSON.parse(localStorage.getItem("notes"));
if (getPostIt) {
    getPostIt.forEach((post) => {
        addPostIt(post); //allow to display post
    });
}

const updateLS = () => {
    const postItText = document.querySelectorAll("textarea");
    const tabPostIt = [];

    postItText.forEach(post => {
        tabPostIt.unshift(post.value);
    });

    localStorage.setItem("notes", JSON.stringify(tabPostIt));
}


/* CLICK */
btnAdd.addEventListener("click", () => {
    addPostIt();
});
