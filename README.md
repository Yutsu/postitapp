# Post It Application
![Design homepage of the post it application](./Images/homepage.png)

## Description
This is an application where you can add a note, a task to do.

## Contains

Keep notes via local storage even if you refresh

Add notes

Delete notes

Update notes


## Technologies Used
Html

Css

Javascript

## Installation
git clone https://gitlab.com/Yutsu/postitapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Moose Photos**

https://www.pexels.com/fr-fr/photo/horloge-grise-a-double-cloche-1037993/
